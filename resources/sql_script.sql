with recursive MpcsAndTheirClients(
id,
parent_id,
brand_display_name,
company_name,
company_country,
"type",
"depth",
relation
)
as
(
select id, parent_id, brand_display_name,company_name,company_country,"type", 0, 'GPI (1)'
from accounts
where "type" = 'GPI'

union all

select
accounts.id,
accounts.parent_id,
accounts.brand_display_name,
accounts.company_name,
accounts.company_country,
accounts."type",
MpcsAndTheirClients."depth" + 1,
MpcsAndTheirClients.relation || ' | ' || accounts."type"|| ' ' || accounts.brand_display_name || '(' || accounts.id || ')'
from accounts
join MpcsAndTheirClients
on accounts.parent_id = MpcsAndTheirClients.id
)
select m.account_id as account_id, u.id as user_id, u.first_name as first_name, u.last_name as last_name,u.country as country, u.status as status, mcp."depth" as depth, mcp.relation as relation, mcp."type" as "type", mcp.brand_display_name as "display_brand_name"
from MpcsAndTheirClients mcp
inner join memberships m
	on mcp.id = m.account_id
inner join users u
	on m.user_id = u.id
where mcp."depth" <> 0
order by 1 asc