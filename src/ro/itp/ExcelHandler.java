package ro.itp;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import sun.java2d.xr.MutableInteger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class ExcelHandler {
  private CellStyle accountHeaderCellStyle;
  private CellStyle userHeaderCellStyle;
  private CellStyle dataCellStyle;
  private CellStyle brandCellStyle;

  void WriteToExcelFile(String filename) throws Exception {

    AccountInfoTreeAggregator aggregator = new AccountInfoTreeAggregator();

    List<TreeNode> childNodes = aggregator.GetNodesDepthOne();

    Workbook workbook = new HSSFWorkbook();

    accountHeaderCellStyle = workbook.createCellStyle();
    Font accountHeaderFont = workbook.createFont();
    accountHeaderFont.setBold(true);
    accountHeaderFont.setColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());
    accountHeaderCellStyle.setFont(accountHeaderFont);
    accountHeaderCellStyle.setFillForegroundColor(IndexedColors.ROYAL_BLUE.getIndex());
    accountHeaderCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    accountHeaderCellStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);

    userHeaderCellStyle = workbook.createCellStyle();
    Font userHeaderFont = workbook.createFont();
    userHeaderFont.setBold(true);
    userHeaderFont.setColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());
    userHeaderCellStyle.setFont(userHeaderFont);
    userHeaderCellStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
    userHeaderCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    userHeaderCellStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);

    dataCellStyle = workbook.createCellStyle();
    dataCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    dataCellStyle.setFillForegroundColor(IndexedColors.WHITE1.getIndex());
    dataCellStyle.setBorderBottom(BorderStyle.HAIR);
    dataCellStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);

    brandCellStyle = workbook.createCellStyle();
    Font brandFont = workbook.createFont();
    brandFont.setBold(true);
    brandFont.setColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
    brandCellStyle.setFont(brandFont);
    brandCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    brandCellStyle.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
    brandCellStyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);

    for (TreeNode childNode : childNodes) {

      Sheet sheet = workbook.createSheet(Integer.toString(childNode.getData().getId()));

      MutableInteger currentRow = new MutableInteger(-1);

      createRowsForChildRecursive(childNode, sheet, currentRow);

      performAutoSizeColumns(sheet);
    }

    ExportResultsToFile(filename, workbook);
  }

  private void createRowsForChildRecursive(TreeNode node, Sheet sheet, MutableInteger currentRow) {

    createAccountHeaderRow(sheet, currentRow);
    createAccountDataRow(node, sheet, currentRow);
    createUserHeaderRow(sheet, currentRow);

    for (User user : node.getData().getUsers()) {
      CreateUserDataRow(sheet, currentRow, user);
    }

    for (TreeNode child : node.getChildren()) {
      createRowsForChildRecursive(child, sheet, currentRow);
    }
  }

  private void ExportResultsToFile(String filename, Workbook workbook) throws IOException {
    File currDir = new File(".");
    String path = currDir.getAbsolutePath();
    String fileLocation = path.substring(0, path.length() - 1) + filename;

    FileOutputStream outputStream = new FileOutputStream(fileLocation);
    workbook.write(outputStream);
    workbook.close();
  }

  private void performAutoSizeColumns(Sheet sheet) {
    sheet.autoSizeColumn(0, true);
    sheet.autoSizeColumn(1, true);
    sheet.autoSizeColumn(2, true);
    sheet.autoSizeColumn(3, true);
    sheet.autoSizeColumn(4, true);
    sheet.autoSizeColumn(5, true);
    sheet.autoSizeColumn(6, true);
    sheet.autoSizeColumn(7, true);
    sheet.autoSizeColumn(8, true);
  }

  private void CreateUserDataRow(Sheet sheet, MutableInteger currentRow, User user) {
    currentRow.setValue(currentRow.getValue() + 1);

    Row userRow = sheet.createRow(currentRow.getValue());

    Cell cellUser1 = userRow.createCell(0);
    cellUser1.setCellStyle(dataCellStyle);
    cellUser1.setCellValue(user.getId());
    Cell cellUser2 = userRow.createCell(1);
    cellUser2.setCellStyle(dataCellStyle);
    cellUser2.setCellValue(user.getFirstName() + ' ' + user.getLastName());
    Cell cellUser3 = userRow.createCell(2);
    cellUser3.setCellStyle(dataCellStyle);
    cellUser3.setCellValue(user.getEmail());
    Cell cellUser4 = userRow.createCell(3);
    cellUser4.setCellStyle(dataCellStyle);
    cellUser4.setCellValue("");
    addDataValidation(sheet, cellUser4.getRowIndex(), cellUser4.getColumnIndex());
  }

  private void addDataValidation(Sheet sheet, int row, int column) {

    CellRangeAddressList addressList = new CellRangeAddressList(row, row, column, column);
    DVConstraint dvConstraint =
        DVConstraint.createExplicitListConstraint(
            new String[] {"Admin", "Owner", "Member", "Inactive", "Demo"});
    DataValidation dataValidation = new HSSFDataValidation(addressList, dvConstraint);
    dataValidation.setSuppressDropDownArrow(false);
    sheet.addValidationData(dataValidation);
  }

  private void createUserHeaderRow(Sheet sheet, MutableInteger currentRow) {
    currentRow.setValue(currentRow.getValue() + 1);

    Row userHeaderRow = sheet.createRow(currentRow.getValue());

    Cell headerCell1 = userHeaderRow.createCell(0);
    headerCell1.setCellStyle(userHeaderCellStyle);
    headerCell1.setCellValue("user_id");
    Cell headerCell2 = userHeaderRow.createCell(1);
    headerCell2.setCellStyle(userHeaderCellStyle);
    headerCell2.setCellValue("name");
    Cell headerCell3 = userHeaderRow.createCell(2);
    headerCell3.setCellStyle(userHeaderCellStyle);
    headerCell3.setCellValue("email");
    Cell headerCell4 = userHeaderRow.createCell(3);
    headerCell4.setCellStyle(userHeaderCellStyle);
    headerCell4.setCellValue("type");
  }

  private void createAccountDataRow(TreeNode childNode, Sheet sheet, MutableInteger currentRow) {
    currentRow.setValue(currentRow.getValue() + 1);

    Row accountRow = sheet.createRow(currentRow.getValue());

    Cell cellAccount0 = accountRow.createCell(0);
    cellAccount0.setCellStyle(brandCellStyle);
    cellAccount0.setCellValue(childNode.getData().getId());
    Cell cellAccount1 = accountRow.createCell(1);
    cellAccount1.setCellStyle(brandCellStyle);
    cellAccount1.setCellValue(childNode.getData().getBrandDisplayName());
    cellAccount1.setCellStyle(brandCellStyle);
    Cell cellAccount2 = accountRow.createCell(2);
    cellAccount2.setCellStyle(brandCellStyle);
    cellAccount2.setCellValue(childNode.getData().getRelation().replaceAll("\\|", "->"));
  }

  private void createAccountHeaderRow(Sheet sheet, MutableInteger currentRow) {
    currentRow.setValue(currentRow.getValue() + 1);

    Row accountHeaderRow = sheet.createRow(currentRow.getValue());

    Cell cellAccountHeader0 = accountHeaderRow.createCell(0);
    cellAccountHeader0.setCellStyle(accountHeaderCellStyle);
    cellAccountHeader0.setCellValue("account_id");
    Cell cellAccountHeader1 = accountHeaderRow.createCell(1);
    cellAccountHeader1.setCellStyle(accountHeaderCellStyle);
    cellAccountHeader1.setCellValue("brand_display_name");
    Cell cellAccountHeader2 = accountHeaderRow.createCell(2);
    cellAccountHeader2.setCellStyle(accountHeaderCellStyle);
    cellAccountHeader2.setCellValue("relation");
  }
}
