package ro.itp;

import java.util.List;
import java.util.stream.Collectors;

public class AccountInfoTreeAggregator {
  private TreeNode root;

  public AccountInfoTreeAggregator() throws Exception {
    AccountInfo rootNode = new AccountInfo();
    rootNode.setRelation("GPI");
    rootNode.setDepth(0);
    rootNode.setId(0);
    root = new TreeNode(rootNode);

    BuildTree();
  }

  public List<TreeNode> GetNodesDepthOne() {
    return root.getChildren();
  }

  private void BuildTree() throws Exception {

    AccountInfoParser parser = new AccountInfoParser();

    List<AccountInfo> accountsInfo = parser.Parse();

    AddLevelOneChildrenToRoot(accountsInfo, root);

    AddRemainingChildren(accountsInfo, root);
  }

  private void AddLevelOneChildrenToRoot(List<AccountInfo> accountInfos, TreeNode root) {
    List<AccountInfo> accountsLevelOne =
        accountInfos.stream()
            .filter(account -> account.getDepth() == 1)
            .collect(Collectors.toList());

    for (AccountInfo account : accountsLevelOne) {
      root.addChild(account);
    }
  }

  private void AddRemainingChildren(List<AccountInfo> accountInfos, TreeNode root) {
    List<AccountInfo> children =
        accountInfos.stream()
            .filter(account -> account.getDepth() > 1)
            .collect(Collectors.toList());

    for (AccountInfo account : children) {
      String relation = account.getRelation();
      String[] splits = relation.split("\\|");

      String parentSplitted = splits[splits.length - 2];
      int parentNodeId = Integer.parseInt(parentSplitted.replaceAll("[\\D]", "").trim());

      TreeNode rootNode = root.findNode(root, parentNodeId);

      if (rootNode != null) {
        rootNode.addChild(account);
      }
    }
  }
}
