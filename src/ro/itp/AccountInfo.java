package ro.itp;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@ToString()
public class AccountInfo {
  private String relation;
  private String brandDisplayName;
  private int depth;
  private int id;
  @ToString.Exclude() private List<User> users = new ArrayList<>();
}
