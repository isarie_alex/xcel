package ro.itp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AccountInfoParser {

  private static User getNewUser(List<String> record) {
    User newUser =
        new User(
            Integer.parseInt(record.get(1)),
            record.get(2),
            record.get(3),
            record.get(4),
            record.get(5),
            record.get(6));
    return newUser;
  }

  public List<AccountInfo> Parse() throws Exception {
    List<List<String>> records = new ArrayList<>();

    try (BufferedReader br = new BufferedReader(new FileReader("resources/mcp_clients.csv"))) {

      String line;
      while ((line = br.readLine()) != null) {
        String[] values = line.split(",");

        records.add(Arrays.asList(values));
      }

      records.remove(0);

      List<AccountInfo> accountInfos = new ArrayList<>();

      for (List<String> record : records) {
        boolean hasAccountInfo =
            accountInfos.stream()
                .anyMatch(account -> account.getId() == Integer.parseInt(record.get(0)));

        if (hasAccountInfo == false) {
          AccountInfo newAccountInfo = new AccountInfo();
          newAccountInfo.setId(Integer.parseInt(record.get(0)));
          newAccountInfo.setRelation(record.get(7));
          newAccountInfo.setBrandDisplayName(record.get(9));
          newAccountInfo.setDepth(Integer.parseInt(record.get(6)));

          newAccountInfo.getUsers().add(getNewUser(record));

          accountInfos.add(newAccountInfo);
        } else {
          AccountInfo existingAccountInfo =
              accountInfos.stream()
                  .filter(account -> account.getId() == Integer.parseInt(record.get(0)))
                  .findFirst()
                  .get();

          existingAccountInfo.getUsers().add(getNewUser(record));
        }
      }
      return accountInfos;
    }
  }
}
