package ro.itp;

import lombok.Data;
import lombok.ToString;

import java.util.LinkedList;
import java.util.List;

@Data
public class TreeNode {
  private AccountInfo data;
  private TreeNode parent;

  @ToString.Exclude private List<TreeNode> children;

  public TreeNode(AccountInfo data) {
    this.data = data;
    this.children = new LinkedList<>();
  }

  public TreeNode addChild(AccountInfo child) {
    TreeNode childNode = new TreeNode(child);
    childNode.setParent(this);

    this.children.add(childNode);
    return childNode;
  }

  public TreeNode findNode(TreeNode node, int id) {
    if (node.getData().getId() == id) {
      return node;
    } else {
      for (TreeNode child : node.getChildren()) {
        TreeNode result = findNode(child, id);
        if (result != null) {
          return result;
        }
      }
    }
    return null;
  }
}
